# Trinity : RNA-Seq De novo transcriptome assembler  #

This image is downloaded from https://github.com/trinityrnaseq/trinityrnaseq/wiki/Trinity-in-Docker (v2.3.2)
docker pull trinityrnaseq/trinityrnaseq
docker login registry.gitlab.com
docker tag trinityrnaseq/trinityrnaseq registry.gitlab.com/jgi/trinity
docker push registry.gitlab.com/jgi/trinity

To run:
docker run --rm -v`pwd`:`pwd` jgi/trinity Trinity \
      --seqType fq --single `pwd`/reads.fq.gz \
      --max_memory 1G --CPU 4 --output `pwd`/trinity_out_dir