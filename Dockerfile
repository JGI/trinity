FROM trinityrnaseq/trinityrnaseq 
LABEL Maintainer Shijie Yao, syao@lbl.gov
COPY grid_conf_SLURM.txt /usr/local/src/trinityrnaseq/.
COPY slurm.conf /usr/local/src/trinityrnaseq/.
RUN chmod 755 /usr/local/src/trinityrnaseq/slurm.conf

COPY write_partitioned_trinity_cmds.pl /usr/local/src/trinityrnaseq/util/support_scripts/.

RUN wget https://github.com/HpcGridRunner/HpcGridRunner/archive/v1.0.2.tar.gz && \
	tar -zxvf v1.0.2.tar.gz && \
	rm -rf v1.0.2.tar.gz 

ENV PATH /usr/local/src/HpcGridRunner-1.0.2/:$PATH
RUN apt-get update
RUN apt-get install --yes vim
